import { createStore } from "vuex";

export default createStore({
  state: {
    site: localStorage.getItem("leadhit-site-id") || null,
  },
  getters: {
    getSite(state) {
      return state.site;
    },
  },
  mutations: {
    setSite(state, id) {
      localStorage.setItem("leadhit-site-id", id);

      state.site = id;
    },
  },
  actions: {},
  modules: {},
});
