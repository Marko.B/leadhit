import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AuthView from "@/views/AuthView";
import AnalyticsView from "@/views/AnalyticsView";
import store from "@/store";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/auth",
    name: "auth",
    component: AuthView,
  },
  {
    path: "/analytics",
    name: "analytics",
    component: AnalyticsView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from) => {
  if (store.getters.getSite && to.name === "auth") {
    return { name: "analytics" };
  } else if (!store.getters.getSite && to.name === "analytics") {
    return { name: "auth" };
  } else {
    return true;
  }
});

export default router;
